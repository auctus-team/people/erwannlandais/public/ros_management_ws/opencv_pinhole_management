#ifndef PINHOLE_UTILS_H_
#define PINHOLE_UTILS_H_

#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>

#include "sensor_msgs/CameraInfo.h"

struct TimerParams
{
    int caracSizeX;
    int backSizeY;
    int posXText;
    int posYText;
};

class pinholeUtils
{
    public:
        pinholeUtils();

        void from3Dto2D(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point2d> & all2Dpoints);

        void from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints);

        void getInfosFromCI(sensor_msgs::CameraInfo msg);


        // Apply a supplementary transformation to points
        void from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints,
                                    Eigen::Affine3d camInWorld);

       void from3Dto2D(std::vector<cv::Point3d> all3Dpoints, cv::Mat Ts, cv::Mat rvecRs, std::vector<cv::Point2d> & all2Dpoints);
        
        void from3Dto2DImagePlan(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point3d> & all2DpointsImagePlan);

        void setTimer(double duration, int timSize = -1, std::vector<int> timPos = {});

        void activateTimer(double start_time);

        void updateTimer(cv::Mat & img, double time, bool red = false);

        std::vector<double> stopTimer(double time);

        std::string secondsToString(double time, std::vector<int> & timeDiv);
        
        void blankPicture(cv::Mat & img);


        void setTextOnPicture(cv::Mat & img, std::string text,
    std::string desiredFontColor = "green",
    std::string desiredBackColor = "white");


        void extendTimer(double extendDuration);

        void pauseTimer(bool pauseOrRestartTimer, double time);

        double startTimerTime = 0.0;
        double timerDuration = 0.0;
        bool isTimerActive = false;

        bool isTimerPaused = false;

        double startPauseTime = 0.0;

        std::string timeDisplayed = "";
        double timeLeft = -1.0;

        std::vector<int> timeDiv;
        int timerSize = 4;
        // position of the middle of the textBox
        int midPosX = 0;
        int midPosY = 0;

        double extendedDuration = 0.0;

        int height;
        int width;

        cv::Mat distCoeffs;    
        cv::Mat camMat;            
        cv::Mat rvecR;         
        cv::Mat T;
        cv::Mat K;
        bool gotCamInfo;

        std::map<int,TimerParams> sizeTP;

        std::map<std::string, cv::Scalar> colors;
};

#endif