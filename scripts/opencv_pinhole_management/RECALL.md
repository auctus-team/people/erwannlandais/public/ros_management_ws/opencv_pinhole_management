RECALLS : 
1920x1080 : 
1920 : width
1080 : height
https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html#gafa5aa3f40a472bda956b4c27bec67438

image : M[0:height,0:width] ==> confirmed
image.size() : [1920 x 1080] : [width x height]
x = M[:,i] (in 0:width = 1920)
y = M[i,:] (in 0:height = 1080)
==> cx : on col : width = u
==> cy : on row : height = v

==> u : on col
==> v : on row

Move on x axis ==> stay at same row, move on cols
Move on y axis ==> stay at same col, move on rows

        Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]        

        And 2D points are organized as follows : 
            [x,y]
            [u,v]
            [0:width,0:height]
        