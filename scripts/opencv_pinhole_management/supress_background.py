import numpy as np
import cv2
import copy

if __name__ == "__main__":
    img = cv2.imread("/home/auctus/thesis_ws/src/thesis_management/CR/Reunion_hebdo/redaction_these/figs/etat_art/MyCobot-Camera-Flange-Close-Up.jpg")
    
    cshp = img[0,0].shape
    ## get from all squares
    Nsize = 50

    ishp = img.shape



    Lval = [
        ## up left
        int(np.mean(img[0:Nsize,0:Nsize])),

        int(np.mean(img[int(ishp[0]/2)-Nsize:int(ishp[0]/2)+Nsize,0:Nsize])),


        # ## bot left
        # int(np.mean(img[ishp[0]-Nsize-1:ishp[0]-1,0:Nsize])),
        # ## up right
        # int(np.mean(img[0:Nsize,ishp[1]-Nsize-1:ishp[1]-1])),
        # ## bot right
        # int(np.mean(img[ishp[0]-Nsize-1:ishp[0]-1,ishp[1]-Nsize-1:ishp[1]-1]))
    ]

    for i in range(len(Lval)):

        color = Lval[i]*np.ones(cshp)

        print("Filter color : ", color)


    blank = 255*np.ones(cshp)

    print(blank)

    # print(np.all(img[0,0] == color))

    out = copy.deepcopy(img)
    thres = 10
    # exit()

    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            check = False
            k = 0
            while not check and k < len(Lval):
                color = Lval[k]*np.ones(cshp)

                if np.max(np.abs(img[i,j] - color)) < thres:
                    check = True

                    out[i,j] = blank
                    # for k in range(len(img[i,j])):

                    # print(img[i,j])
                    # exit()
                k+=1
    
    cv2.imwrite("/home/auctus/thesis_ws/src/thesis_management/CR/Reunion_hebdo/redaction_these/figs/etat_art/MyCobot-Camera-Flange-Close-Up_mod.jpg", out)
