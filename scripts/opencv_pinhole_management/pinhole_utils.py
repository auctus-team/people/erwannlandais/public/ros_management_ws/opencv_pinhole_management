import cv2
import numpy as np
import rospy

from sensor_msgs.msg import CameraInfo

import yaml

class pinholeUtils():

    def __init__(self):
        """!
        
        
        camMat : 
        [f'x 0 c'x tx
        0 f'y c'y ty
        0 0 1 tz]

        K : 
        [fx 0 cx
        0 fy cy
        0 0 1]
        
        """
        self.gotCI = False        
        self.height = 0
        self.width = 0
        self.distCoeffs  =None
        self.camMat = None
        self.rvecR = None
        self.T = None
        self.K = None

        self.ci_msg = None

    def get_cam_info_from_yaml(self,yaml_fname):
        with open(yaml_fname, "r") as file_handle:
            calib_data = yaml.load(file_handle)     


        camera_info_msg = CameraInfo()
        camera_info_msg.width = calib_data["image_width"]
        camera_info_msg.height = calib_data["image_height"]
        camera_info_msg.K = calib_data["camera_matrix"]["data"]
        camera_info_msg.D = calib_data["distortion_coefficients"]["data"]
        camera_info_msg.R = calib_data["rectification_matrix"]["data"]
        camera_info_msg.P = calib_data["projection_matrix"]["data"]
        camera_info_msg.distortion_model = calib_data["distortion_model"]               

        return(camera_info_msg)


    def set_cam_info_msg(self,msg):

        if not self.gotCI:
            self.height = msg.height
            self.width = msg.width

            dC = np.array(msg.D)
            cM = np.eye(3,4)

            for i in range(3):
                for j in range(4):
                    cM[i,j] = msg.P[i*4+j]

            L = cv2.decomposeProjectionMatrix(cM)
            K_temp = L[0]
            rvec_temp = L[1]
            Thomogeneous = L[2]

            ## nb : convertPointsFromHomogeneous is a pain in the ass
            ## to work, use numpy functions instead to do the exact
            ## same thing.

            # Th = np.array(Thomogeneous, dtype=np.float32).reshape(4,1)
            # print(Th)            
            # T_temp = cv2.convertPointsFromHomogeneous(Th)

            L = []
            for k in range(3):
                L.append(Thomogeneous[k]/Thomogeneous[3])
            
            T_temp = np.float32(L)

            


            rvecR_temp,_ = cv2.Rodrigues(rvec_temp)

            # print( "cM : ", cM )
            # print( "dC : ", dC)
            # print("Rot_mat : " ,rvec_temp)
            ##print("rVecR : ",rvecR_temp)
            # print("T : ",T_temp)
            # print("K : ",K_temp)


            # save all the matrix (in case we need them later)
            self.distCoeffs = dC
            self.camMat = cM
            self.rvecR = rvecR_temp
            self.T = T_temp
            self.K = K_temp
            self.gotCI = True

            debug2D =  [ [0.0,0.0], [self.width,self.height], [self.width/2, self.height/2]]
            alldepth = [ 0.15, 0.15, 0.25]
            print("debug_check 2D: begin : ",debug2D)
            debug3D = self.from2Dto3DW(debug2D, alldepth)     
            print("2D->3D :", debug3D)            
            debug2D_aft = self.from3Dto2DNS(debug3D)
            print("3D->2D : ", debug2D_aft)

            debug3D =  [ [-0.05,-0.03], [0.3,0.3], [0.0,0.0]]
            alldepth = [0.2,0.7, 0.5]
            for k in range(len(debug3D)):
                debug3D[k].append(alldepth[k])
            print("debug_check 3D: begin : ",debug3D)            
            debug2D = self.from3Dto2DNS(debug3D)
            print("3D->2D :", debug2D)            
            debug3D_aft = self.from2Dto3DW(debug2D,alldepth)
            print("2D->3D : ", debug3D_aft)


    # def save_cam_info_as_yaml(self,cam_name, yaml_name):
    #     calib_dat = dict(
    #         image_width=self.width,
    #         image_height=self.height,
    #         camera_name=cam_name,

    #         position=dict(
    #             x=float(L[0]),
    #             y=float(L[1]),
    #             z=float(L[2]),
    #         ),
    #         orientation=dict(
    #             x=float(L[3]),
    #             y=float(L[4]),
    #             z=float(L[5]),
    #             w=float(L[6]),
    #         ),
    #     )

    #     with open(
    #         yaml_name,
    #         "w",
    #     ) as file:
    #         yaml.dump(calib_dat, file, default_flow_style=False)


    def get_cam_info_msg(self):
        return(self.ci_msg)

    def L2M(self,L, toL = True):
        """!
        
        Matrice output will be at follows : 

        [ 
            [x1,y1,z1],
            [x2,y2,z2],
            [...],
            [xN,yN,zN]
         ]
        
        """
        M = L
        if toL:
            if type(L) is type([]):
                M = np.float32(L)
            elif type(L) is type(np.float32([])) and len(list(L.shape) ) == 1:
                ## we absolutely want the matrix to be of 2-shape
                M = L.reshape(1,L.shape[0])
        return(M)

    def M2L(self,M, toM = True):
        M2 = self.shapeCorrection(M)
        L = M2
        if (toM):
            if type(M) is type(np.array([])):
                L =[]
                for i in range(M2.shape[0]):
                    L2 = []
                    for j in range(M2.shape[1]):
                        L2.append(M2[i,j])
                    L.append(L2)
        return(L)

    def from2Dto3DW(self,all2Dpoints,allDepth, Lout = True):
        """!

        Lout : if true, will return output as list; otherwise, will return
        output as np.array

        Take 2D points as : 
            * [u,v] (width,height) [x,y]

        allDepth : should be as : [z1, z2, ... zN]

        Will returns 3D points as : 
            * [X,Y,Z]

        Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]
        
        """

        all2Dpoints = self.L2M(all2Dpoints)

        all3DPoints = []        
        if self.gotCI:

            f_x = self.K[0,0]
            f_y = self.K[1,1]
            c_x = self.K[0,2]
            c_y = self.K[1,2]  


            points_undistorted = []  

            ## returns points as [u',v'] (so, already in image plane)
            if len(all2Dpoints)>0:
                points_undistorted = cv2.undistortPoints(all2Dpoints,self.K,self.distCoeffs, P=self.camMat)

            points_undistorted = self.shapeCorrection(points_undistorted)
            #print(points_undistorted)
            
            all3DPoints = np.c_[ points_undistorted, np.ones(points_undistorted.shape[0]) ]

            all3DPoints[:,0] = (all3DPoints[:,0]-c_x)/f_x
            all3DPoints[:,1] = (all3DPoints[:,1]-c_y)/f_y            

            for k in range(len(points_undistorted)):
                all3DPoints[k,:] = all3DPoints[k,:]*allDepth[k]

        return(self.M2L(all3DPoints,Lout) )


    def from2Dto3DR(self,all2Dpoints,allDepth, camInWorld, Lout = True ):
        all2Dpoints = self.L2M(all2Dpoints)        
        all3Dpoints = []
        if self.gotCI:
            all3Dpoints = self.from2Dto3DW(all2Dpoints,allDepth, False)
            Mat_C = np.c_[all3Dpoints, np.ones(3)].T
            Mat_ref = camInWorld @ Mat_C
            all3Dpoints= Mat_ref.T[:,:3]

        return(self.M2L(all3Dpoints, Lout) )


    def shapeCorrection(self,M):
        """!
        
        OpenCV python has a really bad tendency to create matrix with a 3-shape dimension
        from np.float32. The role of this code is to convert it back to 2-shape matrix.
        
        
        """
        M2 = np.float32([])
        if len(list(M.shape))>2:
            for k in range(M.shape[0]):
                if (k==0):
                    M2= np.float32( [ [M[0][0][0], M[0][0][1] ] ])
                else:
                    m = np.float32([ [M[k][0][0], M[k][0][1] ] ])
                    M2 = np.r_[ M2,m ]    
        else:
            M2 = M

        return(M2)        


    def from3Dto2DImagePlan(self, all3Dpoints,Lout = True):
        """!
        
        Reexpress 3D pts [X,Y,Z] (in camera frame) as 2D pts in image frame, associated with
        their depth ( [x,y,Z]).
        
        """
        #print("input : ", all3Dpoints)
        all3Dpoints = self.L2M(all3Dpoints) 
        all2DpointsImagePlan = []
        if (self.gotCI):


            f_x = self.K[0,0]
            f_y = self.K[1,1]
            c_x = self.K[0,2]
            c_y = self.K[1,2]              
            #print("3D pt : ", all3Dpoints)
            all2Dpoints = cv2.projectPoints(all3Dpoints, self.rvecR, self.T, self.K, self.distCoeffs)[0]
            #print("img_pts (3D->2D) : ", all2Dpoints)

            all2Dpoints = self.shapeCorrection(all2Dpoints)

            #all2DpointsImagePlan = self.from2Dto3DW(all2Dpoints, [1.0 for k in range(all2Dpoints.shape[0])])

            points_undistorted = cv2.undistortPoints(all2Dpoints,self.K,self.distCoeffs, P = self.camMat)

            all2DpointsImagePlan = self.shapeCorrection(points_undistorted)
            
            all2DpointsImagePlan = np.c_[all2DpointsImagePlan, all3Dpoints[:,2] ]

            all2DpointsImagePlan[:,0] = (all2DpointsImagePlan[:,0]-c_x)/f_x
            all2DpointsImagePlan[:,1] = (all2DpointsImagePlan[:,1]-c_y)/f_y    

            # allDepth = [1.0 for k in range(len(all2Dpoints))]
            # all2DpointsImagePlan = self.from2Dto3DW(all2Dpoints,allDepth, False)
            # all2DpointsImagePlan[:,2] = all3Dpoints[:,2]

        return(self.M2L(all2DpointsImagePlan, Lout) )


    def from3Dto2DNS(self,all3Dpoints, Lout = True):
        """!
        
        Output will be : [ [u1,v1],
                        [u2,v2],
                        [...],
                        [uN,vN]
        ]

        With : u corresponding to height
        v corresponding to width
        
        """
        all3Dpoints = self.L2M(all3Dpoints)         
        all2Dpoints = []
        if self.gotCI:
            all2Dpoints = cv2.projectPoints(all3Dpoints,self.rvecR,self.T, self.K, self.distCoeffs)[0]
        return( self.M2L(all2Dpoints,Lout) )

    def from3Dto2DS(self,all3Dpoints, Ts, rvecRs, Lout = True):
        all3Dpoints = self.L2M(all3Dpoints)         
        all2Dpoints = []
        if self.gotCI:
            all2Dpoints = cv2.projectPoints(all3Dpoints,rvecRs,Ts, self.K, self.distCoeffs)[0]
        return( self.M2L(all2Dpoints,Lout))

    
    def closestDepthFor2DPoint(self, all2DCartpts, depthWSLimits, Lout=True):
        """!
        
        Given 2D points [X,Y] (expressed in camera frame) and the workspace limits,
        find the closest depth to camera frame that is into camera FOV.

        depthWSLimits = [min, max]

        Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]        

        And 2D points are organized as follows : 
            [x,y]
            [u,v]
            [0:width,0:height]
        
        """
        all2DCartpts = self.L2M(all2DCartpts)
        depths = []
        if (self.gotCI):
            f_x = self.K[0,0]
            f_y = self.K[1,1]
            c_x = self.K[0,2]
            c_y = self.K[1,2]              
                        
            for k in range(all2DCartpts.shape[0]):
                cp2D = all2DCartpts[k,:]

                ## saturate waypoint to stay in FOV
                ub = 0.0
                if (cp2D[0] >= 0.0):
                    ub = self.width
                vb = 0.0
                if (cp2D[1] >= 0.0):
                    vb = self.height
                #print(ub,vb)
                depthLim = max(  (f_x*cp2D[0])/(ub-c_x)  , (f_y*cp2D[1])/(vb-c_y)  )
                #print(depthLim)
                depthLim = min( max( depthWSLimits[0], depthLim), depthWSLimits[1] )

                depths.append(depthLim)

        return(self.L2M(depths,Lout))


    def saturateToImagePlane(self,all3Dpts, wSLimits, Lout = True):
        """!
        
        Reexpress 3D pts [X,Y,Z] (in camera frame) as 3D pts that are into camera FOV, at depths limits.
        If a point do not respect those conditions : 
            * try to find a depth at which the point is into camera frame
            * otherwise, go at maximum depth, and saturate position here.

        wSLimits = [min, max]
        min = np.array([xmin, ymin, zmin]) : 3x1

        Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]        

        And 2D points s are organized as follows : 
            [x,y]
            [u,v]
            [0:width,0:height]
        
        """
        all3Dpts = self.L2M(all3Dpts)
        ## first, convert to 2D
        image_pts = []

        if (self.gotCI):

            f_x = self.K[0,0]
            f_y = self.K[1,1]
            c_x = self.K[0,2]
            c_y = self.K[1,2]              
            
            image_pts = self.from3Dto2DNS(all3Dpts, False)
            pixThres = 10.0


            #maxLims2D = [ [0.0,0.0], [self.height,0.0], [0.0, self.width], [self.height,self.width] ]
            maxLims2D = [ [pixThres,pixThres], [self.width-pixThres,self.height-pixThres] ]            
            maxAllDepths = [wSLimits[1][2] for k in range(len(maxLims2D))]
            maxLims3D = self.from2Dto3DW(maxLims2D, maxAllDepths, False)
            
            for k in range(image_pts.shape[0]):
                pt_cons = image_pts[k,:]
                inPlane = (0 <= pt_cons[0] and pt_cons[0] <= self.width)
                inPlane =  (0 <= pt_cons[1] and pt_cons[1] <= self.height) and inPlane
                max_depth_3D = []
                if not inPlane:
                    ## saturate waypoint to stay in FOV
                    ub = 0.0
                    if (all3Dpts[k,0] >= 0.0):
                        ub = self.height
                    vb = 0.0
                    if (all3Dpts[k,1] >= 0.0):
                        vb = self.width
                    #print(all3Dpts)
                    depthLim = max(  (f_x*all3Dpts[k,0])/(ub-c_x)  , (f_y*all3Dpts[k,1])/(vb-c_y)  )
                    ## saturate according to min depth (for points that are really close to optic center)
                    depthLim = max( wSLimits[0][2], depthLim)

                    if (depthLim > wSLimits[1][2]):
                        ## special case where the corresponding point
                        ## is too far away in depth

                        ## saturate depth to limit
                        depthLim = wSLimits[1][2]
                        ## get 2D point at this depth

                        ip_pt = self.from3Dto2DImagePlan(all3Dpts[k,:], False).flatten()
                        
                        max_depth_3D = ip_pt*depthLim
                        max_depth_3D[2] = depthLim

                        ## saturate position in 3D                      
                        for l in range(2):
                            max_depth_3D[l] =  min( max( max_depth_3D[l] , maxLims3D[0][l] ), maxLims3D[1][l])

                    else:
                        ## get 2D point at current depth
                        ip_pt = self.from3Dto2DImagePlan(all3Dpts[k,:], False).flatten()
                        ## reexpress at depth
                        max_depth_3D = ip_pt*depthLim
                        max_depth_3D[2] = depthLim      

                        ## reexpress limits at depth
                        curLims3D = self.from2Dto3DW(maxLims2D, [depthLim, depthLim], False)
                        for l in range(2):
                            max_depth_3D[l] =  min( max( max_depth_3D[l] , curLims3D[0][l] ), curLims3D[1][l])                        
                        
                        
                else:
                    max_depth_3D = all3Dpts[k,:]

                ## saturate waypoint to WS limits
                for l in range(3):
                    max_depth_3D[l] =  min( max( max_depth_3D[l] , wSLimits[0][l] ), wSLimits[1][l])
                #max_depth_3D = np.min( np.max(maxLims2D[0], max_depth_3D), maxLims2D[1] )
                # print(max_depth_3D)
                # print(all3Dpts[k,:])
                all3Dpts[k,:] = max_depth_3D
            
        return(self.L2M(all3Dpts,Lout))




