Package that contains libraries in Cpp and Python with generic useful functions for pinhole management model (ex : from 2D to 3D, then from 3D to 2D, ...).

This package can also be used to check how to export a C++ library in ROS.

To export Python-ROS library, check https://roboticsbackend.com/ros-import-python-module-from-another-package/

# How to use

## Python

from opencv_pinhole_management.pinhole_utils import pinholeUtils


pU = pinholeUtils()

