#include "opencv_pinhole_management/pinhole_utils.h"

// TODO : visp? https://visp-doc.inria.fr/doxygen/visp-daily/tutorial-pose-estimation.html

    /**
     * 
     *  camMat : 
        [f'x 0 c'x tx
        0 f'y c'y ty
        0 0 1 tz]

        K : 
        [fx 0 cx
        0 fy cy
        0 0 1]
        
     * 
    */
    pinholeUtils::pinholeUtils()
    {
        
        gotCamInfo = false;

        int oriTS = timerSize;
        TimerParams tp;
        
        // timerSize = 4
        timerSize = 4;
        tp.caracSizeX = 75;
        tp.backSizeY = 125;
        tp.posXText = 0;
        tp.posYText = 100;
        sizeTP[timerSize] = tp;

        timerSize = oriTS;
        
        // cv::Scalar : BGR[A]
        colors["green"] = cv::Scalar(0,128,0);
        colors["red"] = cv::Scalar(0,0,255);
        colors["white"] = cv::Scalar(255,255,255);

    }


    void pinholeUtils::getInfosFromCI(sensor_msgs::CameraInfo msg)
    {

            height = msg.height;
            width = msg.width;
            // D --> distorsion params
            // K --> Intrinsic camera matrix for the raw (distorted) images.
            // P --> Projection/camera matrix

            cv::Mat1f dC(msg.D.size(),1); 
            cv::Mat1f cM(3,4);

            for (int i = 0; i < msg.D.size();i++)
            {
                dC(i) = msg.D[i];
            }

            for (int row=0; row<3; row++) {
                for (int col=0; col<4; col++) {
                    cM(row, col) = msg.P[row * 4 + col];
                }
            }
            

            cv::Mat1f K_temp(3,3);
            cv::Mat1f rvec_temp(3,3);
            cv::Mat1f Thomogeneous(4,1);

            cv::decomposeProjectionMatrix(cM, K_temp, rvec_temp, Thomogeneous);


            cv::Mat T_temp; // translation vector
            cv::convertPointsFromHomogeneous(Thomogeneous.reshape(4,1), T_temp);
            cv::Mat1f rvecR_temp(3,1);//rodrigues rotation matrix
            cv::Rodrigues(rvec_temp,rvecR_temp);

            // std::cout << "height : " << cM << std::endl;
            // std::cout << "width : " << cM << std::endl;
            // std::cout << "cM : " << cM << std::endl;
            // std::cout << "dC : " << dC << std::endl;
            // std::cout << "Rot_mat : " << rvec_temp << std::endl;
            // std::cout << "rVecR : " << rvecR_temp << std::endl;
            // std::cout << "T : " << T_temp << std::endl;
            // std::cout << "K : " << K_temp << std::endl;

            // T_temp.setTo(cv::Scalar(0.0));
            // rvecR_temp.setTo(cv::Scalar(0.0));

            // T_temp.at<float>(0,0) = 0.0;
            // T_temp.at<float>(1,0) = 0.0;
            // T_temp.at<float>(2,0) = 0.0;   
            // rvecR_temp.at<float>(0,0) = 0.0;
            // rvecR_temp.at<float>(1,0) = 0.0;
            // rvecR_temp.at<float>(2,0) = 0.0;                        


            // save all the matrix (in case we need them later)
            distCoeffs = dC;
            camMat = cM;
            rvecR = rvecR_temp;
            T = T_temp;
            K = K_temp;

            gotCamInfo = true;

    }


    // Project 3D points in world to 2D points in picture 
    // From https://github.com/daviddoria/Examples/blob/master/c%2B%2B/OpenCV/ProjectPoints/ProjectPoints.cxx
    // https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#void%20projectPoints(InputArray%20objectPoints,%20InputArray%20rvec,%20InputArray%20tvec,%20InputArray%20cameraMatrix,%20InputArray%20distCoeffs,%20OutputArray%20imagePoints,%20OutputArray%20jacobian,%20double%20aspectRatio)
    /*
    
            Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]        

        And 2D points are organized as follows : 
            [x,y]
            [u,v]
            [0:width,0:height]
        
    
    */

    void pinholeUtils::from3Dto2D(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point2d> & all2Dpoints)
    {
        if (gotCamInfo)
        {
            cv::projectPoints(all3Dpoints, rvecR, T, K, distCoeffs, all2Dpoints);
        }
    }

    void pinholeUtils::from3Dto2D(std::vector<cv::Point3d> all3Dpoints, cv::Mat Ts, cv::Mat rvecRs, std::vector<cv::Point2d> & all2Dpoints)
    {
        if (gotCamInfo)
        {
            cv::projectPoints(all3Dpoints, rvecRs, Ts, K, distCoeffs, all2Dpoints);
        }
    }

    // From https://stackoverflow.com/questions/51272055/opencv-unproject-2d-points-to-3d-with-known-depth-z
    /*
    
            Recall : a image matrix is organized as follows : 

             M[0:height,0:width]; 
             M[v,u]
             M[y,x]        

        And 2D points are organized as follows : 
            [x,y]
            [u,v]
            [0:width,0:height]
        
    
    */

    void pinholeUtils::from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints) {
        if (gotCamInfo)
        {
            // intrinsics : K
            // distorsion : distCoeffs
            float f_x = K.at<float>(0, 0);
            float f_y = K.at<float>(1, 1);
            float c_x = K.at<float>(0, 2);
            float c_y = K.at<float>(1, 2);

            //std::cout << f_x << " ; " << f_y << " ; " << c_x << " ; " << c_y << std::endl;


            // Step 1. Undistort
            std::vector<cv::Point2d> points_undistorted;

            if (!all2Dpoints.empty()) {
                cv::undistortPoints(all2Dpoints, points_undistorted, K,
                                distCoeffs, cv::noArray(), camMat);
                // cv::undistortPoints(all2Dpoints, points_undistorted, K,
                //                     distCoeffs);       
            }

            // for (int i =0; i < points_undistorted.size(); i++)
            // {
            //     std::cout << points_undistorted[i].x << " ; " << points_undistorted[i].y << std::endl;
            // }

            // Step 2. Reproject
            //std::vector<cv::Point3d> result;
            //all3DPoints.reserve(all2Dpoints.size());
            for (size_t idx = 0; idx < points_undistorted.size(); ++idx) {
                const double z = allDepth[idx];
                all3Dpoints.push_back(
                    cv::Point3d(  ( (points_undistorted[idx].x - c_x) / f_x) * z,
                                ( ( points_undistorted[idx].y - c_y) / f_y )* z, z) );
            }
        }

    }


    void pinholeUtils::from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints,
                                    Eigen::Affine3d camInWorld)
    {
        if (gotCamInfo)
        {
            from2Dto3D(all2Dpoints,allDepth,all3Dpoints);

            for (int i = 0; i < all3Dpoints.size(); i++)
            {
                Eigen::Vector3d pt_cam(all3Dpoints[i].x,all3Dpoints[i].y,all3Dpoints[i].z);
                Eigen::Vector3d pt_world = camInWorld * pt_cam;
                all3Dpoints[i].x = pt_world(0,0);
                all3Dpoints[i].y = pt_world(1,0);
                all3Dpoints[i].z = pt_world(2,0);                        
            }
        }
 
    }    

    void pinholeUtils::from3Dto2DImagePlan(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point3d> & all2DpointsImagePlan)
    {
        if (gotCamInfo)
        {
            std::vector<cv::Point2d> all2Dpoints;
            // first, from 3D to 2D
            cv::projectPoints(all3Dpoints, rvecR, T, K, distCoeffs, all2Dpoints);
            // then, undistort
            std::vector<cv::Point2d> points_undistorted;

            if (!all2Dpoints.empty()) {
                cv::undistortPoints(all2Dpoints, points_undistorted, K,
                                distCoeffs, cv::noArray(), camMat);
    
            }        
            // then, turn it to image plan
            float f_x = K.at<float>(0, 0);
            float f_y = K.at<float>(1, 1);
            float c_x = K.at<float>(0, 2);
            float c_y = K.at<float>(1, 2);
            for (size_t idx = 0; idx < points_undistorted.size(); ++idx) {
            const double z = all3Dpoints[idx].z;
            all2DpointsImagePlan.push_back(
                cv::Point3d(  ( (points_undistorted[idx].x - c_x) / f_x),
                            ( ( points_undistorted[idx].y - c_y) / f_y ), z) );
            }        

        }    
    }

   /**/
   std::string pinholeUtils::secondsToString(double time, std::vector<int> & timeDiv)
   {
    std::stringstream s;
    timeDiv.clear();
    int t_min = int(time)/60;
    int t_sec = int(time)%60;
    timeDiv.push_back( t_min );
    timeDiv.push_back( t_sec );   

    if (t_min < 10) 
    {
        s << "0";
    }
    s << t_min  << ":";
    if (t_sec < 10)
    {
        s << "0";
    }
    s << t_sec;
    return(s.str());
   }

    /**
     * Set : 
     *  * duration (s)

     * Optionnal : 
     *      - Size of timer
     *      - Position of text : <posX, posY>

     * 
     * 
    */
   void pinholeUtils::setTimer(double duration, int timSize, std::vector<int> timPos)
   {
    timerDuration = duration;
    if (timSize =!-1)
    {
        timerSize = timSize;
    }
    if (timPos.size() == 2)
    {
        midPosX = timPos[0];
        midPosY = timPos[1];
       // std::cout << midPosX << std::endl;
      //  std::cout << midPosY << std::endl;
    }

   }


    void pinholeUtils::extendTimer(double extendDuration)
    {
        extendedDuration += extendDuration;

    }
    /**
     * Argument : 
     *  - True : pause timer
     *  - False : restart timer
    */
    void pinholeUtils::pauseTimer(bool pauseOrRestartTimer, double time)
    {
        if (isTimerActive)
        {
            isTimerPaused = pauseOrRestartTimer;
            if (isTimerPaused)
            {
                startPauseTime = time;
            }
            else if (startPauseTime != 0.0)
            {
                extendTimer( time-startPauseTime );
                startPauseTime = 0.0;
            }
        }
    }


    void pinholeUtils::activateTimer( double start_time)
    {
        startTimerTime = start_time;  
        timeDisplayed = secondsToString(timerDuration, timeDiv );              
        timeLeft = timerDuration;
        startPauseTime = 0.0;
        extendedDuration = 0.0;
        isTimerActive = true;
        isTimerPaused = false;
    }
    /**
     * Return : 
     *  - Time left : (could be negative) : time left on the timer
     *  - Timer duration : duration chosen
     *  - Time spent from the start of the timer to its stop.
     *  - Added duration to timer
     * 
    */
    std::vector<double> pinholeUtils::stopTimer(double time)
    {

        std::vector<double> results;
        timeLeft = std::max(timeLeft,0.0);
        results.push_back(timeLeft);
        results.push_back(timerDuration);
        results.push_back(time - startTimerTime);
        results.push_back(extendedDuration);

        isTimerActive = false;
        return(results);

    }

    void pinholeUtils::setTextOnPicture(cv::Mat & img, std::string text,
    std::string desiredFontColor,
    std::string desiredBackColor)
    {

        if (colors.find(desiredFontColor) == colors.end())
        {
            desiredFontColor = "green";
        }

        if (colors.find(desiredBackColor) == colors.end())
        {
            desiredBackColor = "white";
        }

        TimerParams tp = sizeTP[timerSize];

        int backSizeX = text.size()*tp.caracSizeX;
        backSizeX = std::min(backSizeX, img.size().width);

        int posX = midPosX - backSizeX/2;
        int posY = midPosY - tp.backSizeY/2;

        // put part of picture in color

        int maxPosX =  std::max( std::min(img.size().width-backSizeX-1, posX ), 0); 
        int maxPosY = std::max( std::min(img.size().height -  tp.backSizeY - 1,posY ), 0);
                
        double Ytext = std::max( maxPosY + tp.posYText,100   );

        img(cv::Rect(maxPosX, maxPosY, backSizeX, tp.backSizeY)).setTo( colors[desiredBackColor]);


        cv::putText(img, //target image
                        text, //text
                        cv::Point(maxPosX + tp.posXText, Ytext), //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        timerSize,
                        colors[desiredFontColor], //font color
                        4);             
    }

    /**
     * Parameters : 
     *  - img : image to be modified
     *  - time : current time (in seconds)
     *  Got from https://stackoverflow.com/questions/46500066/how-to-put-a-text-in-an-image-in-opencv-c
     * https://stackoverflow.com/questions/67097553/how-to-color-a-region-of-an-image-in-c
    */
    void pinholeUtils::updateTimer(cv::Mat & img, double time, bool red)
    {
        if (isTimerActive && !isTimerPaused)
        {
           // std::cout << "New time : " << time << std::endl;
            timeLeft = timerDuration - (time - startTimerTime) + extendedDuration;

          //  std::cout << "Time left : " << timeLeft << std::endl;

            if (timeLeft >= 1)
            {
                timeDisplayed = secondsToString(timeLeft, timeDiv );

                //isTimerActive = false;
                //timeLeft = 0.0;
            }
            else if (timeLeft >=0)
            {
                // desactivate timer
                timeLeft = 0.0;
                timeDisplayed = secondsToString(timeLeft, timeDiv );
                
            }

        }

        TimerParams tp = sizeTP[timerSize];

        int backSizeX = timeDisplayed.size()*tp.caracSizeX;
        backSizeX = std::min(backSizeX, img.size().width);

        int posX = midPosX - backSizeX/2;
        int posY = midPosY - tp.backSizeY/2;



        // put part of picture in color

        int maxPosX =  std::max( std::min(img.size().width-backSizeX-1, posX ), 0); 
        int maxPosY = std::max( std::min(img.size().height -  tp.backSizeY - 1,posY ), 0);
                

        // 




        double Ytext = std::max( maxPosY + tp.posYText,100   );

        bool chooseRed = red;


        if (timeLeft < timerDuration/4 && timeLeft > 0)
        {
            chooseRed = true;
        }
        else if (timeLeft == 0.0 || timeDisplayed == "TIMEOUT")
        {
            chooseRed = true;

        }






        if (chooseRed)
        {

            // poss 1 : fond blanc, texte rouge
            // (pas très visible)

            // img(cv::Rect(maxPosX, maxPosY, backSizeX, tp.backSizeY)).setTo(cv::Scalar(255, 255, 255));

            // cv::putText(img, //target image
            //             timeDisplayed, //text
            //             cv::Point(maxPosX + tp.posXText, Ytext), //top-left position
            //             cv::FONT_HERSHEY_DUPLEX,
            //             timerSize,
            //             CV_RGB(255,0,0), //font color
            //             4);

            // poss 2 : fond rouge, texte blanc
            // (pas très visible)
            
            img(cv::Rect(maxPosX, maxPosY, backSizeX, tp.backSizeY)).setTo(colors["red"]);

            cv::putText(img, //target image
                        timeDisplayed, //text
                        cv::Point(maxPosX + tp.posXText, Ytext), //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        timerSize,
                        CV_RGB(255,255,255), //font color
                        4);

        }
        else
        {
            // poss 1 : fond blanc, texte noir
            // (pas très visible)

            // cv::putText(img, //target image
            // timeDisplayed, //text
            // cv::Point(maxPosX + tp.posXText, Ytext), //top-left position
            // cv::FONT_HERSHEY_DUPLEX,
            // timerSize,
            // CV_RGB(0,0,0), //font color
            // 4);

            // poss 2 : fond vert, texte blanc


            img(cv::Rect(maxPosX, maxPosY, backSizeX, tp.backSizeY)).setTo( colors["green"]);

            cv::putText(img, //target image
                        timeDisplayed, //text
                        cv::Point(maxPosX + tp.posXText, Ytext), //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        timerSize,
                        CV_RGB(255,255,255), //font color
                        4);                        
        }

        


    }

    void pinholeUtils::blankPicture(cv::Mat & img)
    {
        img.setTo(cv::Scalar(0,0,0));
    }
